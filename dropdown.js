function init_dropdown() {
    var height = 50;
    $(".options").on('mousedown', function(e) {
        e.preventDefault();
        this.blur();
        window.focus();
        $(this).removeClass("select_selected");
        $(this).addClass("select_selected");
        $(".dropdown_container_full").removeClass("dropdown_hide");
        $("#dropdown").empty();
        $("#dropdown").append('<div class="droption"></div>');

        var selected = $(this).children("option:selected").val();
        var index;
        $(this).find("option").each(function(i) {
            if ($(this).val() == selected) {
                index = i;
                var item = '<div class="droption seted" value="' + $(this).val() + '">' + $(this).val() + '</div>';
            }
            var item = '<div class="droption" value="' + $(this).val() + '">' + $(this).val() + '</div>';
            $("#dropdown").append(item);
        });
        $("#dropdown").append('<div class="droption"></div>');
        $($('.droption')[1]).addClass("seted");

        $('#dropdown').scrollTop((index) * height);

        init();
    });

    $('.dropdown_container_full').click(function(e) {
        if (e.target !== this)
            return;
        if (!$(".dropdown_container_full").hasClass("dropdown_hide")) {
            $(".dropdown_container_full").addClass("dropdown_hide");
        }

    });

    $('#dropdown').scroll(function(e) {
        var step = Math.round(e.delegateTarget.scrollTop / height) + 1;
        ($('.droption')).removeClass("seted");
        ($('.droption')).removeClass("step_more");
        ($('.droption')).removeClass("step_minus");
        $($('.droption')[step]).addClass("seted");
        $($('.droption')[step + 1]).addClass("step_more");
        $($('.droption')[step - 1]).addClass("step_minus");
    });

    $('#dropdown').scroll(_.debounce(function() {
        var scroll = $('#dropdown').scrollTop();
        var step = Math.round(scroll / height);
        $(this).animate({
            scrollTop: (step * height)
        }, 100);
    }, 250));

    $("#btn_dropdown_accept").click(selectOption);

    function selectOption() {
        var scroll = $('#dropdown').scrollTop();
        var step = Math.round(scroll / height) + 1;
        $(".select_selected").val($($('.droption')[step]).attr('value'));
        $(".select_selected").removeClass("select_selected");
        $(".dropdown_container_full").addClass("dropdown_hide");
    }

    $("#btn_dropdown_cancel").click(function() {
        $(".dropdown_container_full").addClass("dropdown_hide");
        $(".select_selected").removeClass("select_selected");
    });


    function init() {
        $(".droption").click(function() {
            if ($(this).hasClass("seted")) {
                selectOption();
            }
        });

        var scroll = $('#dropdown').scrollTop();
        var step = Math.round(scroll / height) + 1;
        ($('.droption')).removeClass("seted");
        ($('.droption')).removeClass("step_more");
        ($('.droption')).removeClass("step_minus");
        $($('.droption')[(step + 1)]).addClass("step_more");
        $($('.droption')[(step - 1)]).addClass("step_minus");
        $($('.droption')[step]).addClass("seted");
    }
}
init_dropdown();