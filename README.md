# Select Html con estilo android/iOS Device

Solo funciona sobre la etiqueta select.

Para implementarlo es necesario que el select posea la clase "options" y ejecutar la función incluida en el archivo "dropdown.js".

El estilo depende de que el div con la clase "dropdown_container_full" tenga como propiedad la clase "android_style" o "ios_style".

Para su correcto funcionamiento el archivo javascript depende de otras librerias tales como:

1. Bootstrap(Solo css)
1. JQuery
1. Loadash

<img src="images/AndroidStyle.png" alt="" width="250" />
<img src="images/iOSStyle.png" alt="" width="250" />